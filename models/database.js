const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/timing';

const client = new pg.Client(connectionString);
client.connect();
const query = client.query(
  'CREATE TABLE IF NOT EXISTS worklogitem (worklogitem_id serial PRIMARY KEY, type integer not null, startdate date not null, enddate date);'+
  'CREATE TABLE IF NOT EXISTS itemtype (itemtype_id serial primary key, typename varchar(100), jirabooking boolean);');
query.on('end', () => { client.end(); });
