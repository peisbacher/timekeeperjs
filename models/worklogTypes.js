const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/timing';

const client = new pg.Client(connectionString);
client.connect();
const query = client.query(
  'INSERT INTO itemtype(typename,jirabooking) VALUES(\'normal\', true);'+
  'INSERT INTO itemtype(typename,jirabooking) VALUES(\'pause\', false);'+
  'INSERT INTO itemtype(typename,jirabooking) VALUES(\'holiday\', false);'+
  'INSERT INTO itemtype(typename,jirabooking) VALUES(\'special\', false);'+
  'INSERT INTO itemtype(typename,jirabooking) VALUES(\'training\', true);'
);
query.on('end', () => { client.end(); });
