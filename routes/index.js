var express = require('express');
var router = express.Router();
const pg = require('pg');
const path = require('path');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/timing';


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Philipps Express' });
});

router.post('/api/v1/worklogitems', (req,res,next) =>{
  const results = [];
  console.log("here I am posting");
  // Grab data from http request
  const data = {type: req.body.type, startdate: req.body.startdate};
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Insert Data
    console.log(data.type, data.startdate);
    client.query('INSERT INTO worklogitem(type, startdate) values($1,$2)',
    [data.type, data.startdate]);
    // SQL Query > Select Data
    const query = client.query('SELECT * FROM worklogitem ORDER BY worklogitem_id ASC');
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});

router.get('/api/v1/worklogitems', (req, res, next) => {
  const results = [];
  console.log("here I am getting");
  // Get a Postgres client from the connection pool
  pg.connect(connectionString, (err, client, done) => {
    // Handle connection errors
    if(err) {
      done();
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // SQL Query > Select Data
    const query = client.query('SELECT * FROM worklogitem ORDER BY worklogitem_id ASC;');
    // Stream results back one row at a time
    query.on('row', (row) => {
      results.push(row);
    });
    // After all data is returned, close connection and return results
    query.on('end', () => {
      done();
      return res.json(results);
    });
  });
});


module.exports = router;
